terraform {
    required_providers {
        aws = {
            source = "hashicorp/aws"
            version = "3.59.0"
        }
    }
    backend "local" {
    }
}

provider "aws" {
    region = "eu-central-1"
}
